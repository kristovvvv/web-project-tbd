<?php

session_start();

include("connection.php");
include("functions.php");

// kontrola jestli je user prihlaseny
$user_data = check_login($con);

?>
<link rel="stylesheet" href="style.css">

<html>


<!-- header - ukazuje veci navic, pokud je user prihlaseny -->
<header>
    <a class="active" href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>


<hr>

<main>
    <h2>Welcome to the ENGLISH WORD TESTING page</h2>
    <h5>Vítejte na stránce pro testování anglických slovíček</h5>
</main>

</html>