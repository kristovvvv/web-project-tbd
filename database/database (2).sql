-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 22. led 2021, 13:17
-- Verze serveru: 10.4.16-MariaDB
-- Verze PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `database`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `rightAnswers` int(11) NOT NULL,
  `percent` double NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `tests`
--

INSERT INTO `tests` (`id`, `rightAnswers`, `percent`, `timeStamp`, `userId`) VALUES
(1, 12, 14, '2021-01-22 09:21:03', 25),
(2, 16, 12, '2021-01-22 09:21:05', 25),
(3, 12, 14, '2021-01-22 09:21:06', 25),
(4, 16, 12, '2021-01-22 09:21:07', 25);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `isAdmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `user_id`, `username`, `password`, `time_created`, `isAdmin`) VALUES
(20, 1825848, 'fasfasfasf', '123456Ab.', '2020-12-03 14:39:01', 0),
(22, 86679753, 'tomasjekokot', '123456Ab.', '2020-12-03 15:28:27', 0),
(24, 0, 'kristov', '123456Ab.', '2021-01-06 13:40:44', 1),
(25, 63043682, 'kristovfafas', '$2y$10$/2j66szYpsauDbR4.1R.aOJmcUzCFJR2tsOkWf6wRVf1nqLej4bsa', '2021-01-22 08:58:14', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `words`
--

CREATE TABLE `words` (
  `id` bigint(20) NOT NULL,
  `word_id` bigint(20) NOT NULL,
  `engword` varchar(50) NOT NULL,
  `czechword` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `words`
--

INSERT INTO `words` (`id`, `word_id`, `engword`, `czechword`) VALUES
(1, 0, 'good', 'dobrý'),
(2, 0, 'bad', 'špatný'),
(3, 0, 'apple', 'jablko'),
(4, 0, 'car', 'auto'),
(5, 0, 'tree', 'strom'),
(6, 0, 'banana', 'banán'),
(7, 0, 'square', 'čtverec'),
(8, 0, 'nine', 'devět'),
(9, 0, 'bycicle', 'kolo'),
(10, 0, 'shirt', 'košile'),
(11, 0, 'jumper', 'svetr'),
(12, 0, 'watch', 'hodinky'),
(13, 0, 'water', 'voda'),
(14, 0, 'cup', 'hrnek'),
(15, 0, 'bottle', 'láhev'),
(16, 0, 'frajer', 'kryštof');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pro tabulku `words`
--
ALTER TABLE `words`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
