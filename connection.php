<?php

$dbhost = "localhost";
$dbuser = "root";
$dbname = "database";

if(!$con = mysqli_connect($dbhost, $dbuser, "", $dbname)) {
    die("failed to connect");
}

$mysqli = new mysqli($dbhost, $dbuser, "", $dbname);

class Database {
    private static $instance = null;
    private $mysqli = null;

    private function __construct()
    {
        $dbhost = "localhost";
        $dbuser = "root";
        $dbname = "database";
        $this->mysqli = new mysqli($dbhost, $dbuser, "", $dbname);
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new Database();
        }

        return self::$instance;
    }

    private function columnsToString($columns)
    {
        $rv = "";

        if(count($columns) == 0)
            return "*";

        foreach ($columns as $index => $column) {
            $rv .= ( $index == 0 ? "" : "," ).$column;
        }
        return $rv;
    }

//    private function valuesToString($values)
//    {
//        $rv = "";
//        foreach ($values as $index => $value) {
//            $rv .= ( $index == 0 ? "" : "," )."'{$value}'";
//        }
//        return $rv;
//    }

    private function generateBindings($columns)
    {
        $rv = "";
        foreach ($columns as $index => $column) {
            $rv .= ( $index == 0 ? "" : "," )."?";
        }
        return $rv;
    }

    private function getTypes($columns)
    {
        $rv="";
        foreach ($columns as $column) {
            switch(gettype($column)) {
                case "integer":
                    $rv .= "i";
                    break;
                default:
                    $rv .= "s";
                    break;
            }
        }
        return $rv;
    }

    public function insert($table, $columns, $values)
    {
        $query = "insert into {$table} ( {$this->columnsToString($columns)} ) values ( {$this->generateBindings($columns)} )";
        $types = $this->getTypes($columns);

        $statement = $this->mysqli->prepare($query);
        if($this->mysqli->error)
            error_log($this->mysqli->error);
        else
        {
            $statement->bind_param($types, ...$values);
            $rv = $statement->execute();
            if(!$rv)
                error_log($this->mysqli->error);

            return $rv;
        }
    }

    private function appendWhere($query, $where)
    {
        if(count($where) == 0)
            return $query;

        foreach ($where as $index => $condition) {
            $query.=($index == 0 ? " where " : "")."{$condition[0]} {$condition[1]} ?";
        }
        return $query;
    }

    private function appendOrderBy($query, $orderBy)
    {
        return $orderBy == '' ? $query : "{$query} order by {$orderBy}";
    }

    private function appendLimit($query, $limit)
    {
        return $limit == -1 ? $query : "{$query} limit {$limit}";
    }

    public function select($table, $columns = [], $where = [], $orderBy = '', $limit = -1)
    {
        $rv = [];
        $query = "select {$this->columnsToString($columns)} from {$table}";
        $whereValues = array_map(function($value){
            return $value[2];
        }, $where);
        $types = $this->getTypes($where);
        $query = $this->appendWhere($query, $where);
        $query = $this->appendOrderBy($query, $orderBy);
        $query = $this->appendLimit($query, $limit);
        $statement = $this->mysqli->prepare($query);

        if($this->mysqli->error) {
            error_log($this->mysqli->error);
            return false;
        }

        if(count($where) > 0)
            $statement->bind_param($types, ...$whereValues);

        $statement->execute();
        $result = $statement->get_result();
        while($row = $result->fetch_assoc())
        {
            array_push($rv, $row);
        }
        return $rv;
    }

    public function delete($where)
    {

    }
}