<?php
session_start();

include("connection.php");
include("functions.php");

$user_data = check_login($con)
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>CONTACT</title>
</head>
<body>

<!-- header - ukazuje veci navic, pokud je user prihlaseny -->
<header>
    <a href="homepage.php">homepage</a>
    <a class="active" href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>
<hr>

<main class="centerText">
    <label>email: <a href="mailto:mullekry@fel.cvut.cz">mullekry@fel.cvut.cz</a> </label> <br>
    <label>creator: <a class="makeThisStrong">Kryštof Müller</a> </label>
</main>

</html>