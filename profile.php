<?php

session_start();

include("connection.php");
include("functions.php");

$user_data = check_login($con);
$display_data = [];

if ($user_data["isAdmin"]==1 and !empty($_GET)) {
    if($_SESSION["id"] != $_GET["id"]) {
        $id = $_GET["id"];
        $query = "select * from users where id = '$id' limit 1";
        $result = mysqli_query($con, $query);
        $display_data = mysqli_fetch_assoc($result);
    }
} else {
    $display_data = $user_data;
}

?>
<link rel="stylesheet" href="style.css">
<html>
<header>
    <a href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php" class="active" >logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>
<hr>

<main class="centerText">
    <a>Uživatelské jméno: <a class="makeThisStrong"><?php echo $display_data["username"] ?> </a></a> <br>
    <a>Datum založení: <a class="makeThisStrong"><?php echo $display_data["time_created"] ?> </a></a> <br>
    <a>Procentuální úspěšnost: <a class="makeThisStrong">tbd</a></a><br>
    <a href='testHistory.php?id=<?php echo $id ?>'>Historie testů</a>
</main>

</html>