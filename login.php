<?php

session_start();

$username = "";

include("connection.php");
include("functions.php");


//login, select from dbs podle username
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    if(!empty($username) && !empty($password)) {
        $result = Database::getInstance()->select('users', [], [['username', '=', $username]]);
        if($result && count($result) > 0) {
            $user = $result[0];
            if(password_verify($password, $user["password"])) {
                $_SESSION["id"] = $user["id"];
                Header("Location: homepage.php");
                die;
            }
        }
        echo "wrong username or password !";
    } else {
        echo "Please enter valid information !";
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login page</title>
</head>
<body>


<link rel="stylesheet" href="style.css">

<!-- header - ukazuje veci navic, pokud je user prihlaseny -->
<header>
    <a href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a class="active" href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>

<hr>


<form action="" method="post" class="centerText">
    <label>username: <input type="text" name="username" value="<?php echo htmlspecialchars($username) ?>"></label><br><br>
    <label>password: <input type="password" name="password"></label><br><br>


    <input type="submit" value="login">
</form>

</body>
</html>
