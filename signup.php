<?php

session_start();

include("connection.php");
include("functions.php");


//register funkce, po uspesne registraci presun na login
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = create_password($_POST["password"]);

    if(!empty($username) && !empty($password)) {
        error_log($password);
        if(Database::getInstance()->insert('users', ['user_id', 'username', 'password', 'isAdmin'], [random_num(8), $username, $password, '0']))
            header("Location: login.php");
        die;
    } else {
        echo "Please enter valid information !";
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>sign up</title>
</head>
<body>
<link rel="stylesheet" href="style.css">

<header>
    <a  href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a class="active"href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>

<hr>


<button id="btn">test js btn</button>

<form action="" method="post" id="regForm" class="centerText">
    <label>username: <input id="usernameinput" type="text" name="username" pattern="[a-zA-Z0-9]{3,20}" title="jméno musí být mezi 3 až 20 znaky, pouze písmena a čísla"</label><br><br>
    <label>password: <input id="firstpwinput" type="password" name="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="heslo musí být dlouhé mezi 8-20 zn."</label><br><br>
    <label>password: <input id="scndpwinput" type="password" name="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="heslo musí být dlouhé mezi 8-20 zn."</label><br><br>

    <input type="submit" value="login" id="submitBtn">
</form>

</body>
</html>

<script src="jsfile.js"></script>