<?php

session_start();
include("connection.php");
include("functions.php");

$user_data = check_login($con);

$results_per_page = 3;

$sql = "select * from users";
$result = mysqli_query($con, $sql);
$number_of_results = mysqli_num_rows($result);

$number_of_pages = ceil($number_of_results/$results_per_page);

if (!isset($_GET['page'])) {
    $page = 1;
} else {
    $page = $_GET['page'];
}

$display_data = [];
if ($user_data["isAdmin"]==1 and !empty($_GET)) {
    if($_SESSION["id"] != $_GET["id"]) {
        $id = $_GET["id"];
        $query = "select * from tests where userId = '$id' limit 1";
        $result = mysqli_query($con, $query);
        $display_data = mysqli_fetch_assoc($result);
    }
} else {
    $display_data = $user_data;
}
?>
<link rel="stylesheet" href="style.css">
<html>

<header>
    <a  href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a> <?php
    } if($user_data["isAdmin"] == 1) { ?>
        <a class="active" href="users.php">USERS</a> <?php
    }
    ?>
</header>

<!-- vypis users z dbs -->
<main>
    <div>
        <table>
            <tr>
                <th>username</th>
                <th>username</th>
                <th>username</th>
            </tr>
            <?php
            $this_page_first_result = ($page-1)*$results_per_page;
            $sql = "select * from tests LIMIT ".$this_page_first_result . ', '.$results_per_page . 'where userId = '.$id;
            $result = mysqli_query($con, $sql);
            while($row = mysqli_fetch_assoc($result)) {
                $id = $row["id"]; ?>
                <tr>
                    <td><a class='makeThisStrong'><?php echo $row['rightAnswers'] ?> / 20</a></td>
                    <td><a><?php echo $row['percent']?> %</a></td>
                    <td><?php echo $row['timeStamp']?></td>
                </tr> <?php } ?>
        </table>
    </div>
</main>

<?php
for ($page=1;$page<=$number_of_pages;$page++) {
    echo '<a href="testHistory.php?page=' . $page . '&id='.$id.'">' . $page . '</a> ';
}

?>

<hr>