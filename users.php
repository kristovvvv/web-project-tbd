<?php

session_start();
include("connection.php");
include("functions.php");

$user_data = check_login($con);

$results_per_page = 3;

$sql = "select * from users";
$result = mysqli_query($con, $sql);
$number_of_results = mysqli_num_rows($result);

$number_of_pages = ceil($number_of_results/$results_per_page);

if (!isset($_GET['page'])) {
    $page = 1;
} else {
    $page = $_GET['page'];
}

?>
<link rel="stylesheet" href="style.css">
<html>

<header>
    <a  href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a href="test.php">test</a>
        <a href="logout.php">log out</a> <?php
    } if($user_data["isAdmin"] == 1) { ?>
        <a class="active" href="users.php">USERS</a> <?php
    }
    ?>
</header>

<!-- vypis users z dbs -->
<main>
    <div>
        <table>
            <tr>
                <th>username</th>
                <th>username</th>
                <th>username</th>
            </tr>
            <?php
            $this_page_first_result = ($page-1)*$results_per_page;
            $sql = "select * from users LIMIT ".$this_page_first_result . ', '.$results_per_page;
            $result = mysqli_query($con, $sql);
            while($row = mysqli_fetch_assoc($result)) {
            $id = $row["id"]; ?>
            <tr>
                <td><a class='makeThisStrong'><?php echo $row['username'] ?></a></td>
                <td><?php echo $row['time_created'] ?> </td>
                <td><a href='profile.php?id=<?php echo $id ?>'>user profile</a></td>
            </tr> <?php } ?>
        </table>
    </div>
</main>

<?php
for ($page=1;$page<=$number_of_pages;$page++) {
    echo '<a href="users.php?page=' . $page . '">' . $page . '</a> ';
}

?>

<hr>