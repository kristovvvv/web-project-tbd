<?php

session_start();

include("connection.php");
include("functions.php");
//login check
$user_data = check_login($con);
$words = Database::getInstance()->select('words', [], [], 'rand()', 5);

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    error_log("Posting");
}

?>

<!doctype html>
<head>
    <title> Retrive data</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<link rel="stylesheet" href="style.css">

<html>

<!-- header - ukazuje veci navic, pokud je user prihlaseny -->
<header>
    <a href="homepage.php">homepage</a>
    <a href="contact.php">kontakt</a>
    <?php if(!check_login($con)) { ?>
        <a href="signup.php">signup</a>
        <a href="login.php">login</a> <?php
    } else { ?>
        <a href="profile.php">logged as: <strong><?php echo $user_data["username"]; ?> </strong></a>
        <a class="active" href="test.php">test</a>
        <a href="logout.php">log out</a>
        <?php
    } if(!empty($user_data)) { if($user_data["isAdmin"] == 1) { ?>
        <a href="users.php">USERS</a> <?php
    } }
    ?>
</header>

<hr>

<!-- form:
levy sloupec: předpis českého slova
pravy sloupec: input pro eng slovo
pole rightAnswers: když se vytvoří české slovíčko v levém sloupci, přidá se anglická verze slova do pole rightAnswers - má sloužit pro porovnávání odeslaných
hodnot po submitu s očekávánou hodnotou (řádky 58-59)
-->
<form action="testResults.php" method="post">
    <table>
        <tr>
            <td>CZECH WORD</td>
            <td>--------------------</td>
            <td>ENGLISH WORD</td>
        </tr>

        <?php
        foreach ($words as $word) {
            echo "<tr>
                    <td>{$word["czechword"]}</td>
                    <td></td>
                    <td><input name=\"{$word["czechword"]}\" type=\"text\" placeholder=\"{$word["engword"]}\"></td>
                  </tr>";
        }
        ?>
    </table>
    <br>
    <input type="submit">
</form>

</body>
